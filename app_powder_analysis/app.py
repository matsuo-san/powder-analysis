import flask as f

app = f.Flask(__name__)

# root
@app.route("/")
def home():
    return f.render_template('home.html')

# infomation of this web app
@app.route("/under_construction")
def under_construction():
    return f.render_template('under_construction.html')

# infomation of this web app
@app.route("/info")
def info():
    return f.render_template('info.html')

if __name__ == '__main__':
    app.debug = True        # debug mode
    app.run(host='0.0.0.0') # access free