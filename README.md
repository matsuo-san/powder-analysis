# powder analysis

## About
* Web app about powder engineering.
* Using flask with Python's programs.

## Installation
```
$ git clone https://gitlab.com/matsuo-san/powder-analysis.git
```

## How to use

```
$ python app_powder_analysis/app.py
```

log in local host using web browser (ex, chrome, Edge, ...)

```
http://localhost:5000/
```
